(function ( $ ) {
	$.fn.circular = function( options ) {
		var $root=$(this);
		var $inner_circle=$root.find('.circle_in');
		var $outher_circle=$root.find('.circle_out');
		var settings = $.extend({
			// These are the defaults.
			inner_r: parseInt($inner_circle.css("width"))/2,
			outher_r: parseInt($outher_circle.css("width"))/2,
			top:parseInt($outher_circle.css("top")),
			left:parseInt($outher_circle.css("left"))
		}, options );
		var center_x=(settings.left+settings.inner_r+settings.outher_r*2);
		var center_y=(settings.top+settings.inner_r+settings.outher_r*2);
		$inner_circle.css({
			'top':(settings.top+settings.outher_r*2)+'px',
			'left':(settings.left+settings.outher_r*2)+'px',
			'width':(settings.inner_r*2)+'px',
			'height':(settings.inner_r*2)+'px'
		});
		$outher_circle.css({
			'width':(settings.outher_r*2)+'px',
			'height':(settings.outher_r*2)+'px'
		})
		.each(function( index ) {
		   var deg=$(this).data('deg');		
			deg = deg* Math.PI / 180;
			cos=Math.cos(deg);
			sin=Math.sin(deg)
			x=(settings.inner_r+settings.outher_r)*Math.cos(deg);
			y=(settings.inner_r+settings.outher_r)*Math.sin(deg) ;
			coord_x=center_x+x;
			coord_y=center_y+y;
			coord_x -=settings.outher_r;
			coord_y -=settings.outher_r;	
			$(this).css({
			'top':(coord_y)+'px',
			'left':(coord_x)+'px'
			});
		});
		
		return this;
	};
}( jQuery ));